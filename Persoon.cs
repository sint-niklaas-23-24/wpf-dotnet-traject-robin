﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace WPF___DotNet_Traject
{
    internal class Persoon
    {
        private string _voornaam;
        private string _achternaam;
        private BitmapImage _avatar;
        private Guid _id;

        public Persoon() { }
        public Persoon(string voornaam, string achternaam, BitmapImage avatar) 
        {
            Voornaam = voornaam;
            Achternaam = achternaam;
            Avatar = avatar;
            _id = Guid.NewGuid();
        }
        public string Voornaam 
        { 
            get { return _voornaam; } 
            set 
            {
                if (value == null || value == string.Empty)
                {
                    throw new Exception("De voornaam werd niet ingevuld!");
                }
                else if (value.Any(char.IsDigit))
                {
                    throw new Exception("De voornaam mag geen cijfer bevatten!");
                }
                else
                {
                    _voornaam = value;
                }
            } 
        }
        public string Achternaam 
        { 
            get { return _achternaam; } 
            set 
            {
                if (value == null || value == string.Empty)
                {
                    throw new Exception("De achternaam werd niet ingevuld!");
                }
                else if (value.Any(char.IsDigit))
                {
                    throw new Exception("De achternaam mag geen cijfer bevatten!");
                }
                else
                {
                    _achternaam = value;
                }
            } 
        }
        public BitmapImage Avatar 
        { 
            get { return _avatar; } 
            set {  _avatar = value; } 
        }
        public Guid Id 
        { 
            get { return _id; } 
        }
        public override bool Equals(object? obj)
        {
            bool resultaat = false;
            if (obj != null)
            {
                if (GetType() == obj.GetType())
                {
                    Persoon r = (Persoon)obj;
                    if (this.Id == r.Id)
                    {
                        resultaat = true;
                    }
                }
            }
            return resultaat;
        }
        public virtual string Details()
        {
            return Voornaam + " " + Achternaam;
        }
    }
}
