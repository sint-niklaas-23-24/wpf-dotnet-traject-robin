﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPF___DotNet_Traject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        List<Persoon> personen = new List<Persoon>();
        Cursist cursist;
        BitmapImage avatar;
        int index;

        private void btnOpslaan_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string voornaam = txtVoornaam.Text;
                string achternaam = txtAchternaam.Text;

                if (avatar == null && BestaatAl() == false)
                {
                    throw new Exception("Er werd nog geen avatar geselecteerd!");
                }
                else if (BestaatAl() == false)
                {
                    cursist = new Cursist(voornaam, achternaam, avatar);
                    personen.Add(cursist);
                    MessageBox.Show($"Welkom {cursist.Voornaam} {cursist.Achternaam}!", "Welkom!", MessageBoxButton.OK);
                    avatar = null;
                }
                else
                {
                    cursist = new Cursist(voornaam, achternaam, avatar);
                    personen[index] = cursist;
                }
                RefreshListBoxes();
                RefreshTextboxes();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding!", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void btnAfbeelding_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SelecteerAvatar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void RefreshTextboxes()
        {
            txtAchternaam.Text = string.Empty;
            txtVoornaam.Text = string.Empty;
        }
        private void SelecteerAvatar()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            if (openFileDialog1.ShowDialog() == true)
            {
                Uri uri = new Uri(openFileDialog1.FileName);
                avatar = new BitmapImage(uri);
            }
            else
            {
                throw new Exception("Er werd geen avatar geselecteerd!");
            }
        }
        private void RefreshListBoxes()
        {
            SortList(personen);
            cmbCursisten.ItemsSource = null;
            cmbCursisten.ItemsSource = personen;
            lbCursisten.ItemsSource = null;
            lbCursisten.ItemsSource = personen;
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            cursist = new Cursist("Gunter", "Batsbak", new BitmapImage(new Uri(@"\Images\Pic1.jpg", UriKind.Relative)));
            personen.Add(cursist);
            cursist = new Cursist("Mister", "Man", new BitmapImage(new Uri(@"\Images\Pic3.jpg", UriKind.Relative)));
            personen.Add(cursist);
            cursist = new Cursist("Shaggy", "Van ScoobyDoo", new BitmapImage(new Uri(@"\Images\Pic2.jpg", UriKind.Relative)));
            personen.Add(cursist);
            RefreshListBoxes();
        }
        private void cmbCursisten_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            BestaatAl();
            lbCursisten.SelectedIndex = index;
        }
        private bool BestaatAl()
        {
            bool resultaat = false;

            foreach (Cursist cursist in personen)
            {
                if (cursist.Equals(cmbCursisten.SelectedItem))
                {
                    txtVoornaam.Text = cursist.Voornaam;
                    txtAchternaam.Text = cursist.Achternaam;
                    avatar = cursist.Avatar;
                    index = cmbCursisten.SelectedIndex;
                    resultaat = true;
                    break;
                }
                else
                {
                    RefreshTextboxes();
                }
            }
            return resultaat;
        }
        private void SortList(List<Persoon> origineel)
        {
            personen = origineel.OrderBy(x => x.Voornaam).ThenBy(x => x.Achternaam).ToList();
        }
        private void btnVerwijderen_Click(object sender, RoutedEventArgs e)
        {
            if (cmbCursisten.SelectedIndex == -1)
            {
                MessageBox.Show("Selecteer eerst een cursist vooraleer je er een verwijderd!", "Foutmelding", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                MessageBox.Show($"Vaarwel {personen[index].Voornaam} {personen[index].Achternaam}!", "Vaarwel!", MessageBoxButton.OK);
                personen.RemoveAt(index);
            }
            RefreshListBoxes();
            RefreshTextboxes();
        }
    }
}
