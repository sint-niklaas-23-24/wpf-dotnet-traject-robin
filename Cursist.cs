﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace WPF___DotNet_Traject
{
    internal class Cursist : Persoon
    {
        private Guid _id;

        public Cursist() : base()
        {
            _id = Guid.NewGuid();
        }
        public Cursist(string voornaam, string naam, BitmapImage avatar) : base(voornaam, naam, avatar)
        {
            _id = Guid.NewGuid();
        }
        public Guid Id
        {
            get { return _id; }
        }
        public override string Details() 
        {
            return base.Details() + " met nummer: " + Id;
        }
    }
}
