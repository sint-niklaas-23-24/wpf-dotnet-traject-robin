﻿using System;
using System.Windows.Media.Imaging;

namespace WPF___DotNet_Traject
{
    internal class Werknemer : Persoon
    {
        private Guid _id;
        private double _loon;

        public Werknemer() : base() 
        {
            _id = Guid.NewGuid();
        }
        public Werknemer(string voornaam, string naam, BitmapImage avatar, double loon) : base (voornaam, naam, avatar)
        {
            _id = Guid.NewGuid();
            Loon = loon;
        }
        public Guid Id
        {
            get { return _id; }
        }
        public double Loon
        {
            get { return _loon; }
            set { _loon = value; }
        }
        public override string Details()
        {
            return base.Details() + " met nummer: " + Id + " en loon: " + Loon;
        }
    }
}
